package net.nurma.workshopday2a;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    private Button takePictureButton;
    private ImageView imageView;
    Uri file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        takePictureButton =
                (Button) findViewById(R.id.button_image);
        imageView =
                (ImageView) findViewById(R.id.imageview);

        //Kalau app belum ada izin akses kamera
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            //disactivate tombol
            takePictureButton.setEnabled(false);
            //Tanyakan lagi ke user untuk minta izin
            // akses kamera
            ActivityCompat.requestPermissions(this,
                    new String[] {
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE },
                    0);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        if (requestCode == 0) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED //Akses camera granted
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED //Akses ext storage granted
                ) {
                takePictureButton.setEnabled(true);
            }
        }

    }

    public void takePicture(View view) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE); //Intent buka kamera handphone
        file = Uri.fromFile(getOutputMediaFile()); //Dapatkan URI path foto yang akan diambil
        intent.putExtra(MediaStore.EXTRA_OUTPUT, file); //Tambahan info untuk intent:  path file

        startActivityForResult(intent, 100); //Mulai intent
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) { //Kalau foto berhasil diambil
                imageView.setImageURI(file); //Pass path file foto ke imageView
            }
        }
    }

    private static File getOutputMediaFile(){
        //Buat directory baru di ext storage bernama CameraDemo
        File mediaStorageDir = new File(
                //Directory ext storage untuk pictures
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "CameraDemo");

        //Kalau direktori ext storage tidak ada dan tidak bisa dibuat direktori baru, cancel (return null)
        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        //Dapatkan timestamp
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        ///Set judul file foto menggunakan IMG_[timestamp].jpg
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_"+ timeStamp + ".jpg");
    }

    public void bukaActivityBaru(View view){
        Intent i = new Intent(this, GalleryActivity.class);
        startActivity(i);
    }
}
